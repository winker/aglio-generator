import Vue from 'vue'
import Router from 'vue-router'
import Generator from '@/components/Generator'
import About from '@/components/About'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Generator
    },
    {
      path: '/About',
      name: 'About',
      component: About
    }
  ]
})
